//
//  CreateTaskVC.swift
//  HW10(d&d)
//
//  Created by Nazar Starantsov on 06.11.2019.
//  Copyright © 2019 Nazar Starantsov. All rights reserved.
//

import Foundation
import UIKit

class CreateTaskVC: UIViewController {
    
    lazy var textView: UITextView = {
        let text = UITextView()
        text.font = UIFont.systemFont(ofSize: 20)
        text.translatesAutoresizingMaskIntoConstraints = false
        text.textColor = .systemBackground
        text.backgroundColor = .gray
        text.textColor = .label
        text.layer.cornerRadius = 15
        return text
    }()
    
    lazy var boardPicker: UIPickerView = {
        let picker = UIPickerView()
        picker.translatesAutoresizingMaskIntoConstraints = false
        return picker
    }()
    lazy var sendButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Создать таск", for: .normal)
        button.layer.cornerRadius = 15
            
        return button
    }()
    
    weak var boardViewController: BoardViewController?
    var delegate: BoardDelegateProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .systemGray2
        addSubviews()
        setupConstraints()
        setupGestures()
        
        guard let boardViewController = boardViewController else { fatalError() }
        boardPicker.dataSource = boardViewController
        boardPicker.delegate = boardViewController
    }
    
    func addSubviews() {
        view.addSubview(textView)
        view.addSubview(boardPicker)
        view.addSubview(sendButton)
    }
    
    func setupConstraints() {
        textView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        textView.topAnchor.constraint(equalTo: view.topAnchor, constant: 50).isActive = true
        textView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.8).isActive = true
        textView.heightAnchor.constraint(equalToConstant: 100).isActive = true
        
        boardPicker.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        boardPicker.topAnchor.constraint(equalTo: textView.bottomAnchor).isActive = true
        boardPicker.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.8).isActive = true
        
        sendButton.topAnchor.constraint(equalTo: view.bottomAnchor, constant: -100).isActive = true
        sendButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        sendButton.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        
    }
    
    func setupGestures() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tappedOut))
        view.addGestureRecognizer(tapGesture)
        
        sendButton.addTarget(self, action: #selector(createTask), for: .touchUpInside)
    }
    
    @objc func tappedOut() {
       textView.resignFirstResponder()
    }
    
    @objc func createTask() {
        let taskText = textView.text ?? ""
        let selectedBoardIndex = boardPicker.selectedRow(inComponent: 0)
        delegate?.createTask(with: taskText, and: selectedBoardIndex)
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
}


protocol BoardDelegateProtocol {
    func createTask(with text: String, and boardIndex: Int)
}
