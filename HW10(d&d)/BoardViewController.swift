//
//  BoardViewController.swift
//  HW10(d&d)
//
//  Created by Nazar Starantsov on 06.11.2019.
//  Copyright © 2019 Nazar Starantsov. All rights reserved.
//

import UIKit

class BoardViewController: UIViewController {

    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumInteritemSpacing = 10
        layout.minimumLineSpacing = 10
        layout.sectionInset = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
        layout.scrollDirection = .horizontal
        
        let col = UICollectionView(frame: .zero, collectionViewLayout: layout)
        return col
    }()
    
    var data: [BoardSection] = [
        BoardSection(tasks: [BoardSectionHeader(headerText: "To do"), BoardTask(text: "make this"), BoardTask(text: "make that")]),
        BoardSection(tasks: [BoardSectionHeader(headerText: "In progress"), BoardTask(text: "lol this"), BoardTask(text: "lol that"), BoardTask(text: "lol that1")]),
        BoardSection(tasks: [BoardSectionHeader(headerText: "Done"), BoardTask(text: "ah this"), BoardTask(text: "ah that")])
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addSubviews()
        setupConstraints()
        navigationController?.navigationBar.isHidden = true
    }

    func addSubviews() {
        view.addSubview(collectionView)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.dragDelegate = self
        collectionView.dropDelegate = self
        collectionView.dragInteractionEnabled = true
        collectionView.backgroundColor = .cyan
        collectionView.contentInset = UIEdgeInsets(top: 15, left: 10, bottom: 0, right: 10)
        collectionView.register(TaskCellCollectionView.self, forCellWithReuseIdentifier: "taskCell")
        collectionView.register(TaskHeaderCell.self, forCellWithReuseIdentifier: "headerCell")
    }
    
    func setupConstraints() {
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        collectionView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        collectionView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }

    fileprivate func reoderItems(coordinator: UICollectionViewDropCoordinator, destinationIndexPath: IndexPath, collectionView: UICollectionView) {
        if let item = coordinator.items.first {
            print(item)
            if let sourceIndexPath = item.sourceIndexPath {
                collectionView.performBatchUpdates({
                    let removedItem = self.data[sourceIndexPath.section].tasks.remove(at: sourceIndexPath.item)
                    self.data[destinationIndexPath.section].tasks.insert(removedItem, at: destinationIndexPath.item)
                    
                    collectionView.deleteItems(at: [sourceIndexPath])
                    collectionView.insertItems(at: [destinationIndexPath])
                }, completion: nil) 
                
                // Animate drop
                coordinator.drop(item.dragItem, toItemAt: destinationIndexPath)
            }
        }
    }
}

extension BoardViewController: UICollectionViewDelegate {
    
}

extension BoardViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return indexPath.item == 0 ? CGSize(width: 200, height: 40) : CGSize(width: 200, height: 100)
    }
}

extension BoardViewController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data[section].tasks.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let headerData = self.data[indexPath.section].tasks[indexPath.item] as? BoardSectionHeader {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "headerCell", for: indexPath) as! TaskHeaderCell
            cell.headerText = headerData.headerText
            cell.navigationController = navigationController
            cell.boardViewController = self
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "taskCell", for: indexPath) as! TaskCellCollectionView
            cell.data = self.data[indexPath.section].tasks[indexPath.item] as? BoardTask
            return cell
        }
    }
    
    
}

extension BoardViewController: UICollectionViewDragDelegate {
    func collectionView(_ collectionView: UICollectionView, itemsForBeginning session: UIDragSession, at indexPath: IndexPath) -> [UIDragItem] {
        // Don't do drag for header
        if indexPath.item == 0 { return [] }
        
        let itemProvider = NSItemProvider(object: "\(indexPath)" as NSString)
        let dragItem = UIDragItem(itemProvider: itemProvider)
        dragItem.localObject = data[indexPath.section].tasks[indexPath.row]
        return [dragItem]
    }
    
}

extension BoardViewController: UICollectionViewDropDelegate {
    
    func collectionView(_ collectionView: UICollectionView, performDropWith coordinator: UICollectionViewDropCoordinator) {
        let destinationIndexPath = coordinator.destinationIndexPath
        
        guard let indexPath = destinationIndexPath else {
            return
        }
        
        if coordinator.proposal.operation == .move {
            self.reoderItems(coordinator: coordinator, destinationIndexPath: indexPath, collectionView: collectionView)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, dropSessionDidUpdate session: UIDropSession, withDestinationIndexPath destinationIndexPath: IndexPath?) -> UICollectionViewDropProposal {
        if collectionView.hasActiveDrag {
            return UICollectionViewDropProposal(operation: .move, intent: .insertAtDestinationIndexPath)
        }
        return UICollectionViewDropProposal(operation: .forbidden)
    }
}

extension BoardViewController: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.data.count
    }
    
    
}

extension BoardViewController: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let data = self.data[row].tasks[0] as? BoardSectionHeader
        return data?.headerText
    }
}


extension BoardViewController: BoardDelegateProtocol {
    func createTask(with text: String, and boardIndex: Int) {
        data[boardIndex].tasks.insert(BoardTask(text: text), at: 1)
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.collectionView.reloadData()
        }
    }
}
