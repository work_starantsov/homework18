//
//  Model.swift
//  HW10(d&d)
//
//  Created by Nazar Starantsov on 06.11.2019.
//  Copyright © 2019 Nazar Starantsov. All rights reserved.
//

import Foundation


struct BoardSection {
    var tasks: [BoardSectionElement]
}

protocol BoardSectionElement {}
struct BoardTask: BoardSectionElement {
    var text: String
}
struct BoardSectionHeader: BoardSectionElement {
    var headerText: String
}
