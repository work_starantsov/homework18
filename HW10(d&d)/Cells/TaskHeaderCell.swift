//
//  TaskHeaderCell.swift
//  HW10(d&d)
//
//  Created by Nazar Starantsov on 06.11.2019.
//  Copyright © 2019 Nazar Starantsov. All rights reserved.
//

import Foundation
import UIKit

class TaskHeaderCell: UICollectionViewCell {
    
    lazy var headerLabel: UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.boldSystemFont(ofSize: 20)
        lbl.textColor = .black
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    lazy var plusView: UIImageView = {
        let view = UIImageView()
        view.image = UIImage(named: "plus")?.withRenderingMode(.alwaysTemplate)
        view.tintColor = UIColor.black.withAlphaComponent(0.7)
        view.isUserInteractionEnabled = true
        view.translatesAutoresizingMaskIntoConstraints = false
        view.clipsToBounds = true
        return view
    }()
    
    var navigationController: UINavigationController?
    weak var boardViewController: BoardViewController?
    
    var headerText: String? {
        didSet {
            self.headerLabel.text = self.headerText!
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        addSubviews()
        setupContraints()
        setupGestures()
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func addSubviews() {
        contentView.addSubview(headerLabel)
        contentView.addSubview(plusView)
    }
    
    func setupContraints() {
        headerLabel.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        headerLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 10).isActive = true
        headerLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -40).isActive = true
        headerLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
        
        plusView.centerYAnchor.constraint(equalTo: headerLabel.centerYAnchor, constant: 3).isActive = true
        plusView.rightAnchor.constraint(equalTo: contentView.rightAnchor).isActive = true
        plusView.widthAnchor.constraint(equalToConstant: 40).isActive = true
        plusView.heightAnchor.constraint(equalToConstant: 40).isActive = true
    }
    
    func setupGestures() {
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(plusTapped))
        plusView.addGestureRecognizer(tapGestureRecognizer)
    }
    
    @objc func plusTapped() {
        guard let navigationController = self.navigationController else {
            fatalError("Navigation controller does not exist")
        }
        let createVC = CreateTaskVC()
        createVC.delegate = boardViewController
        createVC.boardViewController = self.boardViewController
        
        navigationController.present(createVC, animated: true)
    }
}
