//
//  TaskCellCollectionView.swift
//  HW10(d&d)
//
//  Created by Nazar Starantsov on 06.11.2019.
//  Copyright © 2019 Nazar Starantsov. All rights reserved.
//

import Foundation
import UIKit

class TaskCellCollectionView: UICollectionViewCell {
    lazy var label: UILabel = {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .label
        label.numberOfLines = 2
        
        return label
    }()
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.label.text = ""
    }
    
    var data: BoardTask? {
        didSet {
            self.label.text = self.data!.text
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubviews()
        layer.cornerRadius = 15
        backgroundColor = UIColor.gray.withAlphaComponent(0.5)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func addSubviews() {
        contentView.addSubview(label)
        label.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        label.leftAnchor.constraint(equalTo: contentView.leftAnchor).isActive = true
        label.rightAnchor.constraint(equalTo: contentView.rightAnchor).isActive = true
        label.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
        label.textAlignment = .center
    }
}
